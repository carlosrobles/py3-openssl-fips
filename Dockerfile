FROM ubuntu:18.04

MAINTAINER carlos.robles@gmail.com

WORKDIR /tmp

# Needed to bypass tzdata timezone screen - sets tzdata to default UTC
ENV DEBIAN_FRONTEND=noninteractive

COPY install_py3_openssl_fips.sh .
RUN ./install_py3_openssl_fips.sh
