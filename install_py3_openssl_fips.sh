#!/bin/bash

set -ex

# install python3 build dependencies
apt-get -y update
apt-get install -y --no-install-recommends \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libncurses5-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev

# install openssl FIPS module build dependencies
apt-get install -y \
    libgdbm-dev \
    libdb5.3-dev \
    libbz2-dev \
    libexpat1-dev \
    liblzma-dev \
    libffi-dev \
    tcl-dev \
    tk-dev \
    fakeroot \
    libcups2-dev \
    libkrb5-dev \
    libyaml-dev \
    devscripts \
    debhelper \
    libqt4-dev \
    curl \
    wget \
    git \
    file \
    cmake

# download and extract source code packages
wget https://www.python.org/ftp/python/3.6.8/Python-3.6.8.tar.xz
wget https://www.openssl.org/source/openssl-fips-2.0.16.tar.gz
wget https://www.openssl.org/source/openssl-1.0.2q.tar.gz
tar -xf Python-3.6.8.tar.xz
tar -xf openssl-fips-2.0.16.tar.gz
tar -xf openssl-1.0.2q.tar.gz

# build openssl fips module and openssl
# symlink system ssl to fips enabled openssl
cd openssl-fips-2.0.16
./config && make && make install
cd ..
cd openssl-1.0.2q
./config shared fips --prefix=/usr/local --openssldir=/usr/local/openssl && make && make install
cd ..
ln -s -f /usr/local/bin/openssl /usr/bin/openssl

# replace system crypto and ssl files with fips enabled ones
mv /usr/lib/x86_64-linux-gnu/libcrypto.so.1.0.0 /lib/x86_64-linux-gnu/old_libcrypto.so.1.0.0
mv /usr/lib/x86_64-linux-gnu/libssl.so.1.0.0 /lib/x86_64-linux-gnu/old_libssl.so.1.0.0
cp /usr/local/lib/libcrypto.so.1.0.0 /lib/x86_64-linux-gnu/
cp /usr/local/lib/libssl.so.1.0.0 /lib/x86_64-linux-gnu/
cp /usr/local/lib/libcrypto.so.1.0.0 /usr/lib/x86_64-linux-gnu/
cp /usr/local/lib/libssl.so.1.0.0 /usr/lib/x86_64-linux-gnu/
ln -s /lib/x86_64-linux-gnu/libssl.so /lib/x86_64-linux-gnu/libssl.sl.1.0.0
ln -s /lib/x86_64-linux-gnu/libcrypto.so /lib/x86_64-linux-gnu/libcrypto.sl.1.0.0
ln -s /usr/lib/x86_64-linux-gnu/libssl.so /usr/lib/x86_64-linux-gnu/libssl.sl.1.0.0
ln -s /usr/lib/x86_64-linux-gnu/libcrypto.so /usr/lib/x86_64-linux-gnu/libcrypto.sl.1.0.0

# build python with openssl fips support
cd Python-3.6.8
export LDFLAGS="-L/usr/local/lib/ -L/usr/local/lib64/"
export LD_LIBRARY_PATH="/usr/local/lib/:/usr/local/lib64/:/usr/lib/x86_64-linux-gnu/"
export CPPFLAGS="-I/usr/local/include -I/usr/local/include/openssl"
./configure --enable-shared --prefix=/usr/local/ && make -j 8 && make install
cd ..

# copy libpython files to system directories
cp /usr/local/lib/libpython3.6m.so.1.0 /usr/lib/x86_64-linux-gnu/
cp /usr/local/lib/libpython3.6m.so.1.0 /lib/x86_64-linux-gnu/

# cleanup
rm -rf Python-3.6.8.tar.xz
rm -rf openssl-1.0.2q.tar.gz
rm -rf openssl-fips-2.0.16.tar.gz
rm -rf Python-3.6.8
rm -rf openssl-1.0.2q
rm -rf openssl-fips-2.0.16
apt-get purge -y --auto-remove \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libncurses5-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libgdbm-dev \
    libdb5.3-dev \
    libbz2-dev \
    libexpat1-dev \
    liblzma-dev \
    libffi-dev \
    tcl-dev \
    tk-dev \
    fakeroot \
    libcups2-dev \
    libkrb5-dev \
    libyaml-dev \
    devscripts \
    debhelper \
    libqt4-dev
rm -rf /var/lib/apt/lists/*
