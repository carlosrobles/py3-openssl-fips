down:
	docker-compose down

build: down
	docker-compose build


.PHONY: down build
